package octatech.service.bloodbank.Activity.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import octatech.service.bloodbank.R;

public class StartUpActivity extends AppCompatActivity {
    ImageView ivLogo, ivDoctorLogo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_up);

        //setting action of Logo and Doctor Logo
        ivLogo = (ImageView) findViewById(R.id.ivStartUpLogo);
        ivDoctorLogo = (ImageView) findViewById(R.id.ivDoctorLogo);

        //animation main logo

        YoYo.with(Techniques.FadeIn)
                .duration(2000)
                .playOn(findViewById(R.id.ivStartUpLogo));

        // animating doctor logo
        YoYo.with(Techniques.Landing)
                .duration(2000)
                .playOn(findViewById(R.id.ivDoctorLogo));

        // Starting LoginSearchButtonPage Activity after 2 sec...
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                final Intent mainIntent = new Intent(getApplicationContext(), LoginSearchButtonPage.class);
                StartUpActivity.this.startActivity(mainIntent);
                StartUpActivity.this.finish();
            }
        }, 2000);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_start_up, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
