package octatech.service.bloodbank.Activity.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.util.Arrays;
import java.util.List;

import fr.ganfra.materialspinner.MaterialSpinner;
import octatech.service.bloodbank.R;


public class SignUpOptionPage extends AppCompatActivity {
    MaterialSpinner spinner,daySpinner,monthSpinner,yearSpinner,divisionSpinner,districtSpinner,upazilaSpinner;
    Button btSignupForm;
    boolean isSelected = false;
    Toolbar toolbar;
    RadioGroup radioGroup;
    RadioButton radioButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_option_page);
        setupToolbar();
        setBloodSpinner();
        setBirthDaySpinners();
        setAreaSpinners();
        setRadioButtons();
        btSignupForm = (Button) findViewById(R.id.btSignUpForm);
        btSignupForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), LoginOptionPage.class);
                startActivity(intent);
            }
        });
    }

    private void setRadioButtons() {
        radioGroup = (RadioGroup) findViewById(R.id.radio);

    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radio_pirates:
                if (checked)
                    // Pirates are the best
                    break;
            case R.id.radio_ninjas:
                if (checked)
                    // Ninjas rule
                    break;
        }
    }

    private void setAreaSpinners() {
        divisionSpinner = (MaterialSpinner) findViewById(R.id.spDivision);
        districtSpinner = (MaterialSpinner) findViewById(R.id.spDistrict);
        upazilaSpinner = (MaterialSpinner) findViewById(R.id.spUpazila);
        String[] division = getResources().getStringArray(R.array.item_division);

        ArrayAdapter<String> divisionAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, division);
        divisionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        divisionSpinner.setAdapter(divisionAdapter);

        divisionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            ArrayAdapter<String> districtApapter = null;
            List<String> divisionList;
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0) {
                    isSelected = true;
                    divisionList = Arrays.asList(getResources().getStringArray(R.array.dhaka_district));
                    districtApapter = new ArrayAdapter<String>(SignUpOptionPage.this, android.R.layout.simple_spinner_item, divisionList);
                    districtApapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    districtSpinner.setAdapter(districtApapter);
                } else if (position == 1) {
                    isSelected = true;
                    divisionList = Arrays.asList(getResources().getStringArray(R.array.Rajshahi_district));
                    districtApapter = new ArrayAdapter<String>(SignUpOptionPage.this, android.R.layout.simple_spinner_item, divisionList);
                    districtApapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    districtSpinner.setAdapter(districtApapter);
                } else if (position == 2) {
                    isSelected = true;
                    divisionList = Arrays.asList(getResources().getStringArray(R.array.chittagong_district));
                    districtApapter = new ArrayAdapter<String>(SignUpOptionPage.this, android.R.layout.simple_spinner_item, divisionList);
                    districtApapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    districtSpinner.setAdapter(districtApapter);
                } else if (position == 3) {
                    isSelected = true;
                    divisionList = Arrays.asList(getResources().getStringArray(R.array.Khulna_district));
                    districtApapter = new ArrayAdapter<String>(SignUpOptionPage.this, android.R.layout.simple_spinner_item, divisionList);
                    districtApapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    districtSpinner.setAdapter(districtApapter);
                } else if (position == 4) {
                    isSelected = true;
                    divisionList = Arrays.asList(getResources().getStringArray(R.array.Sylhet_district));
                    districtApapter = new ArrayAdapter<String>(SignUpOptionPage.this, android.R.layout.simple_spinner_item, divisionList);
                    districtApapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    districtSpinner.setAdapter(districtApapter);
                } else if (position == 5) {
                    isSelected = true;
                    divisionList = Arrays.asList(getResources().getStringArray(R.array.Rangpur_district));
                    districtApapter = new ArrayAdapter<String>(SignUpOptionPage.this, android.R.layout.simple_spinner_item, divisionList);
                    districtApapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    districtSpinner.setAdapter(districtApapter);
                } else if (position == 6) {
                    isSelected = true;
                    divisionList = Arrays.asList(getResources().getStringArray(R.array.Barisal_district));
                    districtApapter = new ArrayAdapter<String>(SignUpOptionPage.this, android.R.layout.simple_spinner_item, divisionList);
                    districtApapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    districtSpinner.setAdapter(districtApapter);
                } else if (position == 7) {
                    isSelected = true;
                    divisionList = Arrays.asList(getResources().getStringArray(R.array.Mymensingh_distirct));
                    districtApapter = new ArrayAdapter<String>(SignUpOptionPage.this, android.R.layout.simple_spinner_item, divisionList);
                    districtApapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    districtSpinner.setAdapter(districtApapter);
                } else {
                    divisionList = Arrays.asList(getResources().getStringArray(R.array.dhaka_district));
                    districtApapter = new ArrayAdapter<String>(SignUpOptionPage.this, android.R.layout.simple_spinner_item, divisionList);
                    districtApapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    districtSpinner.setAdapter(districtApapter);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        districtSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            List<String> upazillaList;
            ArrayAdapter<String> upazillaApapter = null;

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                // Dhaka District Upazillas
                if( isSelected && divisionSpinner.getSelectedItemPosition() == 1 && districtSpinner.getSelectedItemPosition() == 1){
                    upazillaList = Arrays.asList(getResources().getStringArray(R.array.dhaka_upazilla));
                    upazillaApapter = new ArrayAdapter<String>(SignUpOptionPage.this, android.R.layout.simple_spinner_item, upazillaList);
                    upazillaApapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    upazilaSpinner.setAdapter(upazillaApapter);
                }

                //Faridpur District Upazillas
                else if( isSelected && divisionSpinner.getSelectedItemPosition() == 1 && districtSpinner.getSelectedItemPosition() == 2){
                    upazillaList = Arrays.asList(getResources().getStringArray(R.array.faridpur_upazilla));
                    upazillaApapter = new ArrayAdapter<String>(SignUpOptionPage.this, android.R.layout.simple_spinner_item, upazillaList);
                    upazillaApapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    upazilaSpinner.setAdapter(upazillaApapter);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void setBirthDaySpinners() {
        String[] month = getResources().getStringArray(R.array.item_month);
        String[] year = getResources().getStringArray(R.array.year);
        daySpinner = (MaterialSpinner) findViewById(R.id.spBirthdayDay);
        monthSpinner = (MaterialSpinner) findViewById(R.id.spBirthdayMonth);
        yearSpinner = (MaterialSpinner) findViewById(R.id.spBirthdayYear);


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, month);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        monthSpinner.setAdapter(adapter);

        ArrayAdapter<String> yearAdpater = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, year);
        yearAdpater.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        yearSpinner.setAdapter(yearAdpater);



        monthSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                List<String> day = Arrays.asList(getResources().getStringArray(R.array.item_day));
                if (position == 0 || position == 2 || position == 4 || position == 8 || position == 9
                        || position == 11) {
                    ArrayAdapter<String> dayadapter = new  ArrayAdapter<String>(SignUpOptionPage.this, android.R.layout.simple_spinner_item,day);
                    dayadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    daySpinner.setAdapter(dayadapter);
                }
                else if (position == 1) {
                    day = day.subList(0,28);
                    ArrayAdapter<String> dayadapter = new  ArrayAdapter<String>(SignUpOptionPage.this, android.R.layout.simple_spinner_item,day);
                    dayadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    daySpinner.setAdapter(dayadapter);
                }
                else {
                    day = day.subList(0,30);
                    ArrayAdapter<String> dayadapter = new  ArrayAdapter<String>(SignUpOptionPage.this, android.R.layout.simple_spinner_item,day);
                    dayadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    daySpinner.setAdapter(dayadapter);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });





    }

    void setBloodSpinner(){
        String[] blood_group = getResources().getStringArray(R.array.blood_group);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,  blood_group);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner = (MaterialSpinner) findViewById(R.id.spinner);
        spinner.setAdapter(adapter);
    }
    private void setupToolbar(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sign_up_option_page, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
